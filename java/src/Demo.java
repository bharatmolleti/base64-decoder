import java.io.UnsupportedEncodingException;
import org.apache.commons.codec.binary.Base64;

public class Demo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String data1 = "1VtoL2XZFdaU6nXycSnirqKwL+BENHx7NAwjzkXhQDFNNylCnqY9jRhMEUqmK4nR1Zm8kgI8g/fwMviPscbVr4i3/iB80CLDngSvyBDNl2UkwGBCQ5EOJLap3itxPxw2hcukK7DmnTuNVU90b+ZipVW4bL4EYUQcRIau6wMZtfpO6GkntMJh40OAtWR4fhXfja3OcaRzmLVvYzAfj1jwtnN5I1vpJu91Me2yGFs2prMj18VolUvR28WhEwq1HGNAB06lr6M5gtVTCaBZQwEp4fYCNk0MWqNa/+Qslr4rxxpPeLoEZIaW6qtbWNzBkHD9CcgFFAAEn1BRCjaqc8WtRQ==";
		
		try {			
			System.out.println("The data1 is "+data1+", its length is : "+data1.length());
			
			byte[] data = data1.getBytes("UTF-8");
			
			System.out.println("The data is "+data+", its length is : "+data.length);
			
			 byte[] sigBytes = Base64.decodeBase64(data1);
					 
			 System.out.println("The decoded String  is "+sigBytes+" , with length "+sigBytes.length);
			 
			 for(byte c:sigBytes){
				 System.out.format("%d\n", c);
			 }
			 
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
