#include <stdio.h>
#include <unistd.h>
#include <string.h>

typedef char byte;

class Base64 {

	public:
    /**
     * Used to get the number of Quadruples.
     */
    int FOURBYTE;

    /**
     * Byte used to pad output.
     */
    byte PAD;

    /**
     * The base length.
     */
	static const int BASELENGTH = 255;

    // Create arrays to hold the base64 characters
    byte base64Alphabet[BASELENGTH];

    // Populating the character arrays
    Base64()
	{
		FOURBYTE = 4;
		
		PAD = (byte) '=';
		
		//BASELENGTH = 255;
		
        for (int i = 0; i < BASELENGTH; i++) {
            base64Alphabet[i] = (byte) -1;
        }
        for (int i = 'Z'; i >= 'A'; i--) {
            base64Alphabet[i] = (byte) (i - 'A');
        }
        for (int i = 'z'; i >= 'a'; i--) {
            base64Alphabet[i] = (byte) (i - 'a' + 26);
        }
        for (int i = '9'; i >= '0'; i--) {
            base64Alphabet[i] = (byte) (i - '0' + 52);
        }

        base64Alphabet['+'] = 62;
        base64Alphabet['/'] = 63;
    }
	
	 /**
     * Discards any characters outside of the base64 alphabet, per
     * the requirements on page 25 of RFC 2045 - "Any characters
     * outside of the base64 alphabet are to be ignored in base64
     * encoded data."
     *
     * @param data The base-64 encoded data to groom
     * @return The data, less non-base64 characters (see RFC 2045).
     */
    byte* discardNonBase64(byte* data,int length) {
        byte* groomedData = new byte[length];
        int bytesCopied = 0;

        for (int i = 0; i < length; i++) {
            if (isBase64(data[i])) {
                groomedData[bytesCopied++] = data[i];
            }
        }

        byte* packedData = new byte[bytesCopied];

       memcpy(packedData, groomedData, bytesCopied);
		delete groomedData;
		
        return packedData;
    }

    /**
     * Decodes Base64 data into octects
     *
     * @param base64Data Byte array containing Base64 data
     * @return Array containing decoded data.
     */
    byte* decodeBase64(byte* base64Data, int length) {
        // RFC 2045 requires that we discard ALL non-Base64 characters
        base64Data = discardNonBase64(base64Data, length);

        // handle the edge case, so we don't have to worry about it later
        if (length == 0) {
            return new byte[0];
        }

        int numberQuadruple = length / FOURBYTE;
        byte* decodedData = NULL;
        byte b1 = 0, b2 = 0, b3 = 0, b4 = 0, marker0 = 0, marker1 = 0;

        // Throw away anything not in base64Data

        int encodedIndex = 0;
        int dataIndex = 0;
        {
            // this sizes the output array properly - rlw
            int lastData = length;
            // ignore the '=' padding
            while (base64Data[lastData - 1] == PAD) {
                if (--lastData == 0) {
                    return new byte[0];
                }
            }
            decodedData = new byte[lastData - numberQuadruple];
        }

        for (int i = 0; i < numberQuadruple; i++) {
            dataIndex = i * 4;
            marker0 = base64Data[dataIndex + 2];
            marker1 = base64Data[dataIndex + 3];

            b1 = base64Alphabet[base64Data[dataIndex]];
            b2 = base64Alphabet[base64Data[dataIndex + 1]];

            if (marker0 != PAD && marker1 != PAD) {
                //No PAD e.g 3cQl
                b3 = base64Alphabet[marker0];
                b4 = base64Alphabet[marker1];

                decodedData[encodedIndex] = (byte) (b1 << 2 | b2 >> 4);
                decodedData[encodedIndex + 1] =
                    (byte) (((b2 & 0xf) << 4) | ((b3 >> 2) & 0xf));
                decodedData[encodedIndex + 2] = (byte) (b3 << 6 | b4);
            } else if (marker0 == PAD) {
                //Two PAD e.g. 3c[Pad][Pad]
                decodedData[encodedIndex] = (byte) (b1 << 2 | b2 >> 4);
            } else if (marker1 == PAD) {
                //One PAD e.g. 3cQ[Pad]
                b3 = base64Alphabet[marker0];

                decodedData[encodedIndex] = (byte) (b1 << 2 | b2 >> 4);
                decodedData[encodedIndex + 1] =
                    (byte) (((b2 & 0xf) << 4) | ((b3 >> 2) & 0xf));
            }
            encodedIndex += 3;
        }
        return decodedData;
    }

    /**
     * Check octect wheter it is a base64 encoding.
     *
     * @param octect to be checked byte
     * @return ture if it is base64 encoding, false otherwise.
     */
    bool isBase64(byte octect) {
        if (octect == PAD) {
            return true;
        } else if (base64Alphabet[octect] == -1) {
            return false;
        } else {
            return true;
        }
    }

   
};


int main()
{
	printf("\n Hello World \n");
	
	byte* inData = (byte*)"1VtoL2XZFdaU6nXycSnirqKwL+BENHx7NAwjzkXhQDFNNylCnqY9jRhMEUqmK4nR1Zm8kgI8g/fwMviPscbVr4i3/iB80CLDngSvyBDNl2UkwGBCQ5EOJLap3itxPxw2hcukK7DmnTuNVU90b+ZipVW4bL4EYUQcRIau6wMZtfpO6GkntMJh40OAtWR4fhXfja3OcaRzmLVvYzAfj1jwtnN5I1vpJu91Me2yGFs2prMj18VolUvR28WhEwq1HGNAB06lr6M5gtVTCaBZQwEp4fYCNk0MWqNa/+Qslr4rxxpPeLoEZIaW6qtbWNzBkHD9CcgFFAAEn1BRCjaqc8WtRQ==";
	Base64* base = new Base64();	
	
	byte* outData = base->decodeBase64(inData, 344);
	
	//printf("\n %lu ", outData);
	
	printf("\n \n The length of above string is %d , source is (%d) \n", strlen(outData), strlen(inData));
	for( int i = 0; i < 256; i++){
		printf("%d\n", outData[i]);
	}
	
	delete outData;
	
	delete base;
	
	
}